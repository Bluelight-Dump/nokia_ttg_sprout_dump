## qssi-user 12 SKQ1.211006.001 00WW_2_300 release-keys
- Manufacturer: hmd global
- Platform: holi
- Codename: TTG_sprout
- Brand: Nokia
- Flavor: qssi-user
- Release Version: 12
- Id: SKQ1.211006.001
- Incremental: 00WW_2_300
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Nokia/TheThing_00WW/TTG_sprout:12/SKQ1.211006.001/00WW_2_300:user/release-keys
- OTA version: 
- Branch: qssi-user-12-SKQ1.211006.001-00WW_2_300-release-keys-random-text-45471534524932
- Repo: nokia_ttg_sprout_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
